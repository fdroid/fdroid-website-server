
**ARCHIVED: this has been integrated into** https://gitlab.com/fdroid/fdroid-http-fronters

__website-server_ is the Apache instance for running https://f-droid.org
website, as generated from https://gitlab.com/fdroid/fdroid-website-server.

There are some pieces that are not handled by the ansible rules, since
it is so much easier to just run them manually:

* `certbot --apache`
* hostname setup, e.g. /etc/hostname and /etc/hosts
* time zone, e.g. `dpkg-reconfigure tzdata`
* `~/.ssh/authorized_keys`
